import React from 'react';
import cx from 'classnames';

import api from '@api';
import './styles.css';

class List extends React.Component {
  state = {
    list: [],
    loading: false,
    selectedItem: '',
  }

  clickItem = name => this.setState({ selectedItem: name });

  componentDidMount() {
    api.getList()
    .then(list => this.setState({ list: list, loading: false }));

    this.setState({ loading: true });
  }

  render() {
    const { list, selectedItem } = this.state;

    return (
      <div className="list-box">
        <h2 className="title">List</h2>
        <ul className='items'>
          {list.map(name => {
            const isItemSelected = name === selectedItem;
            return <li key={name} className={cx('item', { selected: isItemSelected })} onClick={() => this.clickItem(name)}>{name}</li>
          })}
        </ul>
      </div>
    );
  }
}

export default List;
