import React from 'react';
import cx from 'classnames';
import {
  func,
  bool,
  object,
} from 'prop-types';

import { toTwoDigitsTimeFormat } from '@utils';
import {
  START,
  PAUSE,
  RESET,
} from '@constants';
import { redTime, maxTime } from './constants';
import './styles.css';

class Timer extends React.Component {
  state = {
    isRedTime: false,
    isReady: true,
    isStart: false,
    isPause: false,
    isEnd: false,
  }

  constructor(props) {
    super(props);

    const { time = { min: 0, sec: 0 } } = this.props;
    const { min, sec } = time;

    this.state = { ...this.state, min, sec };
    this.audio = new Audio('/ring.mp3');
  }

  tick = () => {
    let { min, sec, isRedTime } = this.state;

    min = sec === 0 && min > 0 ? min - 1 : min;
    sec = min >= 0 && sec < 1 ? 59 : sec - 1;
    isRedTime = min === 0 && sec <= redTime;

    if (sec === 0 && min === 0) {
      this.end();
    }

    this.setState({
      min,
      sec,
      isRedTime,
    });
  }

  pause = () => {
    clearInterval(this.intervalID);
    this.setState({ isPause: true, isStart: false });
  }

  end = () => {
    clearInterval(this.intervalID);
    this.audio.play();
    this.props.callBack();
    this.setState({ isEnd: true, isStart: false });
  }

  reset = () => {
    clearInterval(this.intervalID);
    this.audio.pause();
    this.audio.currentTime = 0;
    this.setState({
      min: 0,
      sec: 0,
      isStart: false,
      isEnd: false,
      isRedTime: false,
      isReady: true,
    });
  }

  start = () => {
    const { min, sec } = this.state;

    if(min === 0 && sec === 0) {
      return;
    }

    this.tick();
    this.intervalID = setInterval(this.tick, 1000);
    this.setState({
      isStart: true,
      isPause: false,
      isReady: false,
    });
  }

  incMinute = () => {
    let { min } = this.state;

    min = min === maxTime ? 0 : min + 1;
    this.setState({ min });
  }

  decMinute = () => {
    const { min } = this.state;

    if (min === 0) {
      return;
    }

    this.setState({ min: min - 1 });
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  render() {
    const { controls } = this.props;

    const {
      min,
      sec,
      isEnd,
      isStart,
      isPause,
      isReady,
      isRedTime,
    } = this.state;

    return (
      <div className={cx('timer', { 'end-timer': isEnd })}>
        <h2 className='title'>Timer</h2>
        <div className='time'>
          {controls && (
            <>
              {isReady && <span className='arrow arrow__plus' onClick={this.incMinute}>&#9650;</span>}
              {isReady && <span className='arrow arrow__minus' onClick={this.decMinute}>&#9660;</span>}
            </>
          )}
          <span className='min'>{toTwoDigitsTimeFormat(min)}</span>
          <span>:</span>
          <span className={cx('sec', { red: isRedTime })}>{toTwoDigitsTimeFormat(sec)}</span>
        </div>
        {controls && (
          <div className='btn-group'>
            {(isReady || isPause) && <button className='btn' onClick={this.start}>{START}</button>}
            {isStart && <button className='btn' onClick={this.pause}>{PAUSE}</button>}
            {isEnd && <button className='btn' onClick={this.reset}>{RESET}</button>}
          </div>
        )}
      </div>
    );
  }
}

Timer.defaultProps = {
  callBack: () => null,
};

Timer.propTypes = {
  controls: bool,
  start: bool,
  reset: bool,
  callBack: func,
  time: object,
};

export default Timer;
