class Api {
  getList = () =>
    fetch('http://localhost:8000/list', { responseType: 'json' })
    .then(res => res.json());
}

export default new Api();
