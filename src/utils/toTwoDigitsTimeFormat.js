export const toTwoDigitsTimeFormat = num => {
  const str = num > 9 ? '' : '0';
  return str + num;
}
