import React from 'react';

import { List, Timer } from '@components';
import {
  START,
  PAUSE,
  RESET,
} from '@constants';
import './styles.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.refTimer = React.createRef();
  }

  startTimer = () => this.refTimer.current.start();

  pauseTimer = () => this.refTimer.current.pause();

  resetTimer = () => this.refTimer.current.reset();

  render() {
    const time = { min: 1, sec: 3 };

    return (
      <div className="app">
        <List />
        <Timer ref={this.refTimer} time={time} controls />
        <div className="btn-box">
          <button className='btn' onClick={this.startTimer}>{START}</button>
          <button className='btn' onClick={this.pauseTimer}>{PAUSE}</button>
          <button className='btn' onClick={this.resetTimer}>{RESET}</button>
        </div>
      </div>
    );
  }
}

export default App;
