const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "options",
        baseUrl: "./",
        aliases: {
          "@api": "./src/api/index.js",
          "@components": "./src/components/index.js",
          "@constants": "./src/constants/index.js",
          "@containers": "./src/containers/index.js",
          "@utils": "./src/utils/index.js",
        }
      }
    }
  ]
};
